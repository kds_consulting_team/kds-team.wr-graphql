The purpose of GraphQL Writer is to publish or update data points with a specified GraphQL query to the API server that is powered by GraphQL.

## Configuration

Tables from the input mapping will be used as an input for the GraphQL Writer. Each row from the input tables will be converted into a `graphql_variables`and convert each of the row into a variables object. Details regarding how to send GraphQL Queries with Variables can be found [here](https://graphql.org/learn/queries/#variables). 

### Support Attributes
1. url - `string` (`required`)
    - Input Type: `string`
2. request_type - `string`
    1. POST
    2. PUT
    3. PATCH
    - Default - `POST`
3. user_parameters - `json`
    - {"attribute_name": "attribute_value"}
    - Variables can be used in headers and params
    - For any variables need encryption, add `#` in the prefix of the variable's name
        - {"#attribute_name": "attribute_value"}
4. headers - `json`
    - to call variables defined in user_parameters
        - {"attr": "attribute_name"}
5. params - `json`
    - to call variables defined in user_parameters
        - {"attr": "attribute_name"}
6. graphql_query - `string` (`required`)
    - GraphQL query which will be used for the requests
    - The query needs to be `URL encoded`
7. graphql_variables - `list` (`required`)
    - GraphQL variables which be used for the requests
    - All variables configured in this property needs to exist in the tables from the Input Mapping

### Sample Configuration
```
{
    "url": "https://www.testing.com/graphql",
    "request_type": "POST",
    "user_parameters": {
        "#token": "12345",
        "paging": "1"
    },
    "headers": {
        "token_testing": {
            "attr": "#token"
        }
    },
    "params": {
        "page": {
            "attr": "paging"
        }
    },
    "graphql_query": "URL_ENCODED_GRAPHQL_QUERY",
    "graphql_variables": [
        "orderid",
        "order__external_id__c",
        "totalprice",
        "unitprice",
        "quantity"
    ]
}
```

### CURL Request from above Sample Configuration
```
curl --location --request POST 'https://www.testing.com/graphql?page=1' \
--header 'token_testing: 12345' \
--header 'Content-Type: application/json' \
--data-raw '{"query":"URL_ENCODED_GRAPHQL_QUERY","variables":{"orderid":"1234","order__external_id__c":"1234","totalprice":"10.00","unitprice":"5","quantity":"2"}}'
```
