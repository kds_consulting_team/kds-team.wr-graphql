### Support Attributes
1. url - `string` (`required`)
    - Input Type: `string`
2. request_type - `string`
    1. POST
    2. PUT
    3. PATCH
    - Default - `POST`
3. user_parameters - `json`
    - {"attribute_name": "attribute_value"}
    - Variables can be used in headers and params
    - For any variables need encryption, add `#` in the prefix of the variable's name
        - {"#attribute_name": "attribute_value"}
4. headers - `json`
    - to call variables defined in user_parameters
        - {"attr": "attribute_name"}
5. params - `json`
    - to call variables defined in user_parameters
        - {"attr": "attribute_name"}
6. graphql_query - `string` (`required`)
    - GraphQL query which will be used for the requests
    - The query needs to be `URL encoded`
7. graphql_variables - `list` (`required`)
    - GraphQL variables which be used for the requests
    - All variables configured in this property needs to exist in the tables from the Input Mapping

### Sample Configuration
```
{
    "url": "https://www.testing.com/graphql",
    "request_type": "POST",
    "user_parameters": {
        "#token": "12345",
        "paging": "1"
    },
    "headers": {
        "token_testing": {
            "attr": "#token"
        }
    },
    "params": {
        "page": {
            "attr": "paging"
        }
    },
    "graphql_query": "URL_ENCODED_GRAPHQL_QUERY",
    "graphql_variables": [
        "orderid",
        "order__external_id__c",
        "totalprice",
        "unitprice",
        "quantity"
    ]
}
```