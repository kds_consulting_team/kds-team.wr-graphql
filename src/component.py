'''
GraphQL Writer

'''

import logging
import logging_gelf.handlers
import logging_gelf.formatters  # noqa
import sys
import os  # noqa
import datetime  # noqa
import json
import pandas as pd
import requests
from nested_lookup import nested_lookup

from kbc.env_handler import KBCEnvHandler
from kbc.result import KBCTableDef  # noqa
from kbc.result import ResultWriter  # noqa


# configuration variables
KEY_URL = 'url'
KEY_REQUEST_TYPE = 'request_type'
KEY_USER_PARAMETERS = 'user_parameters'
KEY_HEADERS = 'headers'
KEY_PARAMS = 'params'
KEY_GRAPHQL_QUERY = 'graphql_query'
KEY_GRAPHQL_VARIABLES = 'graphql_variables'
KEY_DEBUG = 'debug'

MANDATORY_PARS = [
    KEY_URL,
    KEY_REQUEST_TYPE,
    KEY_USER_PARAMETERS,
    KEY_HEADERS,
    KEY_PARAMS,
    KEY_GRAPHQL_QUERY,
    KEY_GRAPHQL_VARIABLES,
]
MANDATORY_IMAGE_PARS = []

# Default Table Output Destination
DEFAULT_TABLE_SOURCE = "/data/in/tables/"
DEFAULT_TABLE_DESTINATION = "/data/out/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/files/"
DEFAULT_FILE_SOURCE = "/data/in/files/"

# Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

# Disabling list of libraries you want to output in the logger
disable_libraries = []
for library in disable_libraries:
    logging.getLogger(library).disabled = True

if 'KBC_LOGGER_ADDR' in os.environ and 'KBC_LOGGER_PORT' in os.environ:
    logger = logging.getLogger()
    logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
        host=os.getenv('KBC_LOGGER_ADDR'), port=int(os.getenv('KBC_LOGGER_PORT')))
    logging_gelf_handler.setFormatter(
        logging_gelf.formatters.GELFFormatter(null_character=True))
    logger.addHandler(logging_gelf_handler)
    # remove default logging to stdout
    logger.removeHandler(logger.handlers[0])

NOW = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

APP_VERSION = '0.0.4'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        if self.cfg_params.get(KEY_DEBUG, False) is True:
            logger = logging.getLogger()
            logger.setLevel(level='DEBUG')

        try:
            self.validate_config()
            self.validate_image_parameters(MANDATORY_IMAGE_PARS)
        except ValueError as e:
            logging.error(e)
            exit(1)

    def get_tables(self, tables, mapping):
        """
        Evaluate input and output table names.
        Only taking the first one into consideration!
        mapping: input_mapping, output_mappings
        """
        # input file
        table_list = []
        for table in tables:
            if mapping == "input_mapping":
                destination = table["destination"]
            elif mapping == "output_mapping":
                destination = table["source"]
            table_list.append(destination)

        return table_list

    def _fill_in_user_params(self, config_obj, user_params):
        '''
        Replacing attr with user parameters
        '''

        steps_string = json.dumps(config_obj, separators=(',', ':'))
        for key in user_params:
            lookup_str = '{"attr":"' + key + '"}'
            steps_string = steps_string.replace(
                lookup_str, '"' + str(user_params[key]) + '"')

        new_steps = json.loads(steps_string)
        non_matched = nested_lookup('attr', new_steps)

        if non_matched:
            raise ValueError(
                'Some user attributes [{}] specified in configuration '
                'are not present in "user_parameters" field.'.format(non_matched))

        return new_steps

    def validate_json(self, input_type, str_input):
        '''
        Validating if the input is a JSON
        '''

        try:
            output = json.loads(str_input)
        except Exception:
            if str_input != '':
                logging.error(
                    'Please validate JSON format: [{}]'.format(input_type))
                sys.exit(1)
            else:
                output = {}

        return output

    def validate_columns(self, input_columns, config_var):
        '''
        Validate whether the config column names exist in file inputs
        '''

        for variable in config_var:
            if variable not in input_columns:
                logging.error(
                    '[{}] does not exist in the input file. Please validate GraphQL variable inputs.'.format(variable))
                # variable['name']))s
                sys.exit(1)

    def define_datatype(self, datatype, value):
        '''
        Defining the datatype needed for the value
        '''

        if datatype == 'STRING':
            return str(value)
        elif datatype == 'FLOAT':
            return float(value)
        elif datatype == 'INTEGER':
            return int(value)

    def construct_variable_params(self, row, config_var):
        '''
        Constructing configuration variable for request
        '''

        params_var = {}
        for var in config_var:
            # params_var[var] = self.define_datatype(config_var[var], row [var])
            params_var[var] = row[var]

        return params_var

    def update_request(self, request_method, url, headers, params, body):
        '''
        Sending update requests
        '''

        # Data request
        if request_method == 'POST':
            r = requests.post(
                url=url,
                headers=headers,
                params=params,
                data=body
            )
        elif request_method == 'PUT':
            r = requests.put(
                url=url,
                headers=headers,
                params=params,
                data=body
            )
        elif request_method == 'PATCH':
            r = requests.patch(
                url=url,
                headers=headers,
                params=params,
                data=body
            )

        else:
            logging.error(
                'Configured Request Method [{}] is not supported. Please contact support.'.format(request_method))
            sys.exit(1)

        logging.debug(f'Request Response: {r.json()}')

        response_text = json.dumps(r.json())

        # Failing the compmonent if request is not 'successful'
        if r.status_code not in [200, 201, 202]:
            logging.error(
                'Request issue. Please refer to the error: {}'.format(
                    r.raise_for_status())
            )
            response_text = json.dumps(r.raise_for_status())
            # sys.exit(1)

        return r.status_code, response_text

    def output_log(self, df):
        '''
        Outputting log messages
        '''

        log_filename = 'request_log.csv'
        log_file_destination = DEFAULT_TABLE_DESTINATION+log_filename
        log_headers = [
            "source_file",
            "request_datetime",
            "request_query",
            "request_variables",
            "request_status",
            "request_response"
        ]

        if not os.path.isfile(log_file_destination):
            with open(log_file_destination, 'a') as b:
                df.to_csv(b, index=False, columns=log_headers)
            b.close()

            # Manfiest
            manifest = {
                "incremental": True,
                "primary_key": [
                    "source_file",
                    "request_datetime",
                    "request_query",
                    "request_variables"
                ]
            }
            with open(f'{log_file_destination}.manifest', 'w') as m:
                json.dump(manifest, m)
        else:
            with open(log_file_destination, 'a') as b:
                df.to_csv(b, index=False, header=False, columns=log_headers)
            b.close()

    def run(self):
        '''
        Main execution code
        '''
        # Get proper list of tables
        in_tables = self.configuration.get_input_tables()
        in_table_names = self.get_tables(in_tables, 'input_mapping')
        logging.info("IN tables mapped: "+str(in_table_names))

        # Terminate code if no input tables are found
        if len(in_table_names) == 0:
            logging.error('Input Mapping is missing.')
            sys.exit(1)

        # Wrtier configuration parameters
        params = self.cfg_params  # noqa
        url = params.get(KEY_URL)
        request_type = params.get(KEY_REQUEST_TYPE) if params.get(
            KEY_REQUEST_TYPE) else 'POST'
        user_parameters = params.get(KEY_USER_PARAMETERS) if params.get(
            KEY_USER_PARAMETERS) else {}
        headers = params.get(KEY_HEADERS) if params.get(KEY_HEADERS) else {}
        r_params = params.get(KEY_PARAMS) if params.get(KEY_PARAMS) else {}
        graphql_query = params.get(KEY_GRAPHQL_QUERY)
        graphql_variables = params.get(KEY_GRAPHQL_VARIABLES)

        # Validate input params
        if not url or not graphql_query or not graphql_variables:
            raise ValueError(
                'Required Parameters are missing: url, graphql_query, graphql_variables')

        # Reconstruct graphql variables, easier to work with
        graphql_var = graphql_variables
        # for variable in graphql_variables:
        #    graphql_var[variable['name']] = variable['datatype']

        # Validating Input JSONs Parameters
        # headers_json = self.validate_json('Request Headers', headers)
        # params_json = self.validate_json('Request Parameters', r_params)
        # Filling in user parametrs
        headers_json = self._fill_in_user_params(headers, user_parameters)
        params_json = self._fill_in_user_params(r_params, user_parameters)

        if len(graphql_variables) == 0:
            logging.error('Please input GraphQL Variables.')
            sys.exit(1)

        # Forcing Header's Content-Type to GraphQL
        headers_json['Content-Type'] = 'application/graphql'

        logging.debug('Request Type: {}'.format(request_type))
        logging.debug('Request Headers: {}'.format(headers_json))
        logging.debug('Request Parameters: {}'.format(params_json))
        logging.debug('GraphQL Query: {}'.format(graphql_query))

        # Gauge the number of total requests
        total_requests = 0

        # Read Input tables
        for table in in_tables:
            logging.info('Fetching details in [{}]'.format(
                table['destination']))

            # Reading Input file in chunks
            for data_in in pd.read_csv(table['full_path'], chunksize=100):
                # Logging all request to the Server
                request_log = []

                table_columns = list(data_in.columns)
                self.validate_columns(table_columns, graphql_var)

                for index, row in data_in.iterrows():
                    request_params_variables = self.construct_variable_params(
                        row=row, config_var=graphql_var)
                    logging.debug('Request Parameters Variables: {}'.format(
                        request_params_variables))

                    # Request Parameters for this row
                    # row_params = params_json
                    # row_params['variables'] = json.dumps(
                    #     request_params_variables)
                    # print(graphql_query)

                    # Request Body for this row
                    row_body = {}
                    row_body['query'] = graphql_query
                    row_body['variables'] = request_params_variables

                    request_status, response = self.update_request(
                        request_method=request_type,
                        url=url,
                        headers=headers_json,
                        # params=row_params,
                        params=params_json,
                        # body=graphql_query
                        body=json.dumps(row_body)
                    )

                    # Logging all of the requests going through
                    request_log_row = {
                        "source_file": table['destination'],
                        "request_datetime": NOW,
                        "request_query": graphql_query,
                        "request_variables": request_params_variables,
                        "request_status": request_status,
                        "request_response": response
                    }
                    request_log.append(request_log_row)

                    total_requests += 1
                    if total_requests % 100 == 0:
                        logging.info(
                            'Number of requests pushed: {}'.format(total_requests))

                out_log = pd.DataFrame(request_log)
                self.output_log(out_log)

        logging.info('Total requests pushed: {}'.format(total_requests))

        logging.info("GraphQL Writer finished")


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug = sys.argv[1]
    else:
        debug = True
    comp = Component(debug)
    comp.run()
